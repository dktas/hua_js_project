const db = {
	connection: null,
	init: function () {
		const mysql = require('mysql');
		this.connection = mysql.createConnection({
			host: '35.238.233.8',
			user: 'nodejs',
			password: 'katsdimi',
			//database: 'myfirstdatabase'
			database: 'moviesdb'
		});
		this.connection.connect();
	},
	query: function (q, callback) {
		this.connection.query(q, function (error, results, fields) {
			if (error) {
				callback(error);
			}
			callback(null, results);
		});
	}

};

module.exports = db;