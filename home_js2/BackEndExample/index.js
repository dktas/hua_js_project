const express = require('express');
var bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json({ type: 'application/json' }));

//start the web server on port 3000 
app.listen(3000);

//map HTTP calls to specific URLs to corresponding code
const routes = require('./routes');
routes.configure(app);

//initialize the DB connection
const db = require('./connection');
db.init();