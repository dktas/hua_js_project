const contacts = require('./contacts');
const movies = require('./movies');
const ratings = require('./ratings');

module.exports = {
	configure:function(app){
		app.use(function (req, res, next) {

			// Website you wish to allow to connect
			res.setHeader('Access-Control-Allow-Origin', "*");
		
			// Request methods you wish to allow
			res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		
			// Request headers you wish to allow
			res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
		
			// Set to true if you need the website to include cookies in the requests sent
			// to the API (e.g. in case you use sessions)
			res.setHeader('Access-Control-Allow-Credentials', true);
		
			// Pass to next layer of middleware
			next();
		});
		app.get('/contacts', async function (req, res) {
			try{
				const result = await contacts.getAllContacts();	//works async with only the rest of the code in try block. Whatever else is out of the try/catch block runs sunchronously
				res.json(result);
			}catch(err){
				throw err;
			}
		});

		app.get('/contacts/:id', async function (req, res) {
			try{
				const result = await contacts.getContactsById(req.params.id);
				res.json(result);
			}catch(err){
				throw err;
			}
		});

		
		app.post('/movies', async function (req, res) {
			try{
				var keyword= req.body.keyword;
				const result = await movies.getAllMovies(keyword);	//works async with only the rest of the code in try block. Whatever else is out of the try/catch block runs sunchronously
				res.json(result);
			}catch(err){
				throw err;
			}
		});

		app.get('/movies/:id', async function (req, res) {
			try{
				//var idS = req.params.id;
				const result = await movies.getMoviesById(req.params.id);
				res.json(result);
			}catch(err){
				throw err;
			}
		});

		app.post('/ratings', async function (req, res) {
			try{
				var movieList= req.body.movieList;

				const result = await ratings.getAllRatings(movieList);	//works async with only the rest of the code in try block. Whatever else is out of the try/catch block runs sunchronously
				res.json(result);
			}catch(err){
				throw err;
			}
		});

		app.get('/ratings/:id', async function (req, res) {
			try{
				const result = await ratings.getRatingsById(req.params.id);
				res.json(result);
			}catch(err){
				throw err;
			}
		});
	}
};