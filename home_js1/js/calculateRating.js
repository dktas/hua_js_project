//Array με 2 τιμές απο τις οποίες στη πρώτη θα αποθηκεύται ο πιο κοντά σε εμάς χρήστης και στη δεύτερη το correlation  του που θα κυμαίνεται μεταξύ -1 και 1 (εμείς ψάχνουμε τη τιμή πιο κοντά στο 1)
var bestuser = [0, -1];
//Array με τα ratings των ταινιών που ψηφίσαμε
var userRating = [];
//Array που περιέχει arrays στα οποία αποθηκεύονται οι ψήφοι του κάθε χρήστη
var ratingtocompare = [];
//Array στο οποίο αποθηκέυονται όλοι οι μοναδικοί χρήστες που ψήφισαν κοινές ταινίες με εμάς
var userstocompare = [];

//Array με τα movieId των ταινιών που έχει βαθμολογήσει ο χρήστης πιο κοντά σε μας
var suggestionId = [];
//Array με τα ratings των ταινιών που έχει βαθμολογήσει ο χρήστης πιο κοντά σε μας 
var suggestionrating = [];

//Function που χρησιμοποιεί έναν αλγόριθμο ο οποίος συγκρίνει για κάθε χρήστη που έχει ψηφίσει κοινές ταινίες με έμας, τις δικές μας ψήφους με τις δικίες του.
//Επιστρέφει μια τιμή μεταξύ -1 και 1 όπου για 1 έχουμε ψηφίσει ίδιο αριθμό κοινών ταινιών και με ίδιες βαθμολογίες  για -1 ίδιο αριθμό κοινών ταινιών και με τελείως αντίθετες βαθμολογίες και για 0 δεν έχουμε ψηφίσει κοινές ταινίες
//https://memory.psych.mun.ca/tech/js/correlation.shtml με κάποιες αλλαγές
function getPearsonCorrelation(x, y) {
    for (k = 0; k < userstocompare.length; k++) {
        var chosenuser = userstocompare[k];
        y = ratingtocompare[k];
        console.log(y.length);
        if (userRating.length > 2) {
            var shortestArrayLength = 0;
            if (x.length == y.length) {
                shortestArrayLength = x.length;
            } else if (x.length > y.length) {
                shortestArrayLength = y.length;
                console.error('x has more items in it, the last ' + (x.length - shortestArrayLength) + ' item(s) will be ignored');
            } else {
                shortestArrayLength = x.length;
                console.error('y has more items in it, the last ' + (y.length - shortestArrayLength) + ' item(s) will be ignored');
            }
            console.log(shortestArrayLength);
            var xy = [];
            var x2 = [];
            var y2 = [];
            for (var i = 0; i < shortestArrayLength; i++) {
                xy.push(x[i] * y[i]);
                x2.push(x[i] * x[i]);
                y2.push(y[i] * y[i]);
            }
            console.log(xy);
            console.log(x2);
            console.log(y2);

            var sum_x = 0;
            var sum_y = 0;
            var sum_xy = 0;
            var sum_x2 = 0;
            var sum_y2 = 0;

            for (var i = 0; i < shortestArrayLength; i++) {
                sum_x += Number(x[i]);
                sum_y += Number(y[i]);
                sum_xy += xy[i];
                sum_x2 += x2[i];
                sum_y2 += y2[i];
            }
            console.log(sum_x);
            console.log(sum_xy);
            console.log(sum_y2);
            console.log(sum_y);
            console.log(sum_x2);
            console.log(shortestArrayLength);

            var step1 = (shortestArrayLength * sum_xy) - (sum_x * sum_y);
            var step2 = (shortestArrayLength * sum_x2) - (sum_x * sum_x);
            var step3 = (shortestArrayLength * sum_y2) - (sum_y * sum_y);
            var step4 = Math.sqrt(step2 * step3);
            var answer = step1 / step4;
            console.log(step1);
            console.log(step2);
            console.log(step3);
            console.log(step4);
            console.log(answer);
            //Στο τέλος κάθε loop του αλγορίθμου ελέγχουμε αν η τιμή μας είναι πιο κοντά στο 1 απο τη προηγούμενη επιλέγοντας 
            //έτσι το χρήστη πιο κοντά σε μας
            if (answer > bestuser[1]) {
                bestuser = [];
                bestuser.push(chosenuser, answer);
            }
            console.log(bestuser);
            suggestBtn.style.display = "block";
            storeBtn.style.display = "none";
        } else {
            document.getElementById("Sug").innerHTML = "<h2><b>Please vote at least three movies!</b></h2>";
        }
    }
}
//Function που για τον χρήστη πιο κοντά σε μας βρίσκει όλα τα movieId και ratings των ταινιών που έχει ψηφίσει
function userSuggestion() {
    //Ζητάμε αριθμό ψήφων πάνω απο 3,5 για να έχουμε καλύτερα αποτελέσματα
    if (userRating.length > 3,5) {
        Sug.style.display = "none";
        loader.style.display = "block";
        document.getElementById("VotesStored").innerHTML = "<h2><b>Your votes have been stored!</b></h2>";
    }
    var xhr = new XMLHttpRequest();
    var UsertoSuggest = bestuser[0];
    xhr.onload = function () {
        if (this.status == 200) {
            loader.style.display = "none";
            var moviestosuggest = JSON.parse(this.responseText);
            console.log(moviestosuggest);
            for (var i = 0; i < moviestosuggest.length; i++) {
                suggestionId.push(moviestosuggest[i]["movieId"]);
                suggestionrating.push(moviestosuggest[i]["rating"]);
            }
        }
        console.log(suggestionId);
        console.log(moviestosuggest);
        console.log(suggestionrating);
    };
    //xhr.open("GET", 'http://62.217.127.19:8010/ratings/' + UsertoSuggest, true);
    xhr.open("GET", 'http://35.204.79.57:3000/ratings/' + UsertoSuggest, true);
    xhr.send();
}
//Function που για κάθε ID ταινίας που έχει ψηφίσει ο χρήστης πιο κοντά σε μας βρίσκει το τίτλο της  
//και σε ποιά κατηγορία ταινίας ανήκει για να τα εμφανίσουμε ως προτάσεις σε ένα πίνακα
function findSugMovies() {
    MovieSuggestions.style.display = "block";
    Votes.style.display = "none";
    Sug.style.display = "none";
    rateBtn.style.display = "none";
    suggestBtn.style.display = "none";
    loader.style.display = "block";
    var movieCount = 0;
    for (i = 0; i < suggestionId.length; i++) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            if (this.status == 200) {
                loader.style.display = "none";
                var moviestosuggest2 = JSON.parse(this.responseText);
                var output = '';
                movieCount++;
                //Λόγω μεγάλου όγκου προτάσεων αναζητάμε μόνο αυτες που ο χρήστης κοντά σε μας έχει ψηφίσει με 5 και σταματάμε να εμφανίζουμε προτάσεις αν αυτές ξεπεράσουν τις 100
                if (suggestionrating[movieCount] = 5) {
                    if (movieCount <= 100) {
                        output += '<td>' + movieCount + '</td>' +
                            '<td>' + moviestosuggest2[0]["movieId"] + '</td>' +
                            '<td>' + moviestosuggest2[0]["title"] + '</td>' +
                            '<td>' + moviestosuggest2[0]["genres;"] + '</td>' +
                            '<td>' + suggestionrating[movieCount] + '</td>';
                    }
                }
                document.getElementById("Content2").innerHTML += output;
            }
        };
       //xhr.open("GET", 'http://62.217.127.19:8010/movie/' + suggestionId[i], true);
        xhr.open("GET", 'http://35.204.79.57:3000/movies/' + suggestionId[i], true);
        xhr.send();
    }
}