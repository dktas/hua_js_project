//Τα function active και inactive είναι για αισθητικούς λόγους ώστε οταν πατάμε το searchbar να αχνοφαίνεται η επιλογή search for movie... , να εξαφανίζεται οταν γράψουμε κατι και να επανεμφανίζεται οταν είναι κενή.
function active(searchBtn) {
    var searchBar = document.getElementById('searchBar');
    if (searchBar.value == 'Search for movie...') {
        searchBar.value = '';
        searchBar.placeholder = 'Search for movie...';
    }
}

function inactive(searchBtn) {
    var searchBar = document.getElementById('searchBar');
    if (searchBar.value == '') {
        searchBar.value = 'Search for movie...';
        searchBar.placeholder = '';
    }
}