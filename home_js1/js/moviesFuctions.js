//Array με τα Ids των ταινιών που ψηφίσαμε  
var userMovieId = [];

//Array με objects τα οποία αναφέρονται σε UserId,MovieId,rating,timestamp για κάθε χρήστη που έχει ψηφίσει έστω και 1 ίδια ταινία με εμάς
var jsonObj = [];
//Array με τα ratings των ταινιών που ψηφίσαμε
var userRating = [];

//Function για την αναζήτηση ταινιών
function searchMovie(searchBtn) {
    var xhr = new XMLHttpRequest();
    var searchInput = document.getElementById("searchBar").value;
    //xhr.open("POST", 'http://62.217.127.19:8010/movie', true);
    //xhr.open("POST", 'http://127.0.0.1:3000/movies', true); //prosthiki search value  
    xhr.open("POST", 'http://35.204.79.57:3000/movies', true);
    //Δεν χρειάζεται xhr.readyState check γιατί το  xhr.onload λειτουργεί μόνο αν το  xhr.readyState == 4: request finished and response is ready
    xhr.onload = function () {
        loader.style.display = "block";
        if (this.status == 200) {
            loader.style.display = "none";
            var movies = JSON.parse(this.responseText);
            var ratingArray = [];
            document.getElementById("moviesFound").innerHTML = movies.length;
            //Το name των βαθμολογιών απο 1 εως 5 πρέπει να αλλάζει κάθε φορά για να μην αποθηκεύει την τιμή του προηγούμενου
            for (var i = 0; i < movies.length; i++) {
                document.getElementById("star1").name = movies[i]["movieId"];
                document.getElementById("star2").name = movies[i]["movieId"];
                document.getElementById("star3").name = movies[i]["movieId"];
                document.getElementById("star4").name = movies[i]["movieId"];
                document.getElementById("star5").name = movies[i]["movieId"];
                ratingArray[i] = document.getElementById("rating").innerHTML;
            }
        }
        for (var i = 0; i < movies.length; i++) {
            var output = '';
            var movieCount = i + 1;
            output += '<td>' + movieCount + '</td>' +
                '<td>' + movies[i]["movieId"] + '</td>' +
                '<td>' + movies[i]["title"] + '</td>' +
                '<td>' + movies[i]["genres;"] + '</td>' +
                '<td>' + ratingArray[i] + '</td>';
            document.getElementById("Content").innerHTML += output;
        }
        var x = document.getElementById("moviesFound").innerHTML;
        if (x > 0) {
            found.style.display = "block";
            mFound.style.display = "none";
        }
        else {
            mFound.style.display = "block";
            document.getElementById("mFound").innerHTML = "<h2><b>No movies found!Try again!</b></h2>";
            found.style.display = "none";
        }
    };

    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify({
        "keyword": "" + searchInput + ""

    }));
    document.getElementById("Votes").innerHTML = "";
}
//Καθαρίζει το table με τα αποτελέσματα για μια νέα αναζήτηση
function clearResults(Content) {
    document.getElementById("Content").innerHTML = "";
    document.getElementById("moviesFound").innerHTML = "";
}

//function που μας επιστρέφει όλους τους χρήστες που έχουν ψηφίσει κοινές ταινίες με εμας , τις ταινίες αυτές και τις βαθμολογίες τους
function rateMovie(rateBtn) {
    storeBtn.style.display = "block";
    loader.style.display = "block";
    var xhr = new XMLHttpRequest();
    mList = '{"movieList":' + JSON.stringify(userMovieId) + '}';

    xhr.onload = function () {
        if (this.status == 200) {
            loader.style.display = "none";
            jsonObj = JSON.parse(xhr.responseText);
            if (userRating.length == 0) {
                document.getElementById("Votes").innerHTML = "<h2><b>You should vote at least one movie!</b></h2>";
            }
            else {
                Votes.style.display = "none";
                document.getElementById("Votes").innerHTML = "<h2><b>You have successfully voted!</b></h2>";
            }
            console.log(jsonObj);
        }
    };

    //xhr.open("POST", "http://62.217.127.19:8010/ratings", true);
    //xhr.open("POST", "http://127.0.0.1:3000/ratings", true);
    xhr.open("POST", "http://35.204.79.57:3000/ratings", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(mList);
}

//Function που αποθηκεύει τα movieId και ratings των ταινιών που ψηφίσαμε σε arrays
function Voting(obj) {
    var x = document.getElementById("moviesFound").innerHTML;
    userMovieId.push(obj.name);
    userRating.push(obj.value);

    console.log(userMovieId);
    console.log(userRating);
}
//Function που αποθηκεύει για κάθε χρήστες τις βαθμολογίες του δίνοντας μοναδικότητα στους χρήστες οι οποίοι μπορεί να έχουν πάνω απο μια κοινή ταινία με εμάς
function suggestMovie() {
    const movieresults = jsonObj.reduce((map, obj) => {
        // set a new key based on the user id if it doesn't exist
        if (!map.has(obj.userId)) {
            map.set(obj.userId, []);
        }
        // add to the users scores to the users score
        map.get(obj.userId).push(obj.rating);
        // return the accumulator
        return map;
    }, new Map);
    for (let [userId, rating] of movieresults.entries()) {
        ratingtocompare.push(rating);
        userstocompare.push(userId);
    }
    console.log(ratingtocompare);
    console.log(userstocompare);
}

function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    console.log(row) ;
    row.parentNode.removeChild(row);
  }